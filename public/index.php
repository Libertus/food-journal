<?php
include_once ('paths.php');
include_once ($offroot.'class.printToBrowser.php');
$printToBrowser = new printToBrowser;

$page = $_GET['page'];
if (''==$page) $page = 'index';

switch ($page) {
	case 'index':
		$printToBrowser->printHomepage();
		break;
	case 'flickr';
		$printToBrowser->printFlickr($_GET['error'],$_GET['username']);
		break;
	case 'picasa';
		$printToBrowser->printPicasa();
		break;		
	case 'about';
		$printToBrowser->printAbout();
		break;			
	case 'news';
		$printToBrowser->printNews();
		break;			
		
}
?>
