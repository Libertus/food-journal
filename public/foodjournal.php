<?php
// force use of local proxy by underlying HTTP API requests
/*
$opts = array('http' => array('request_fulluri' => true,
                              'proxy' => 'tcp://localhost:8123',
                              'header' => array()));

// allow revalidation if requested by browser
$headers = apache_request_headers();
if( 'no-cache' == $headers['Cache-Control'] )
  $opts['header'][] = 'Cache-Control: no-cache';

libxml_set_streams_context( stream_context_get_default($opts) );
*/
include_once ('paths.php');
include_once ($offroot.'class.user.php');
$username = $_GET['username'];
$provider = $_GET['provider'];

$thisfile = $_SERVER['SCRIPT_NAME'];
$n = stripos($thisfile,'foodjournal.php');
$thisfileURL = $_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];
$errorURL = $_SERVER['SERVER_NAME'].substr_replace($thisfile,'error.php',$n);
$indexURL = $_SERVER['SERVER_NAME'].substr_replace($thisfile,'index.php',$n);
//echo $errorURL;

//if(''==$username) $username = 'vege foodie'; // default user with data
//if(''==$provider) $provider = 'flickr';

if (!is_object($user)) $user = User::getGuest( $username , $provider);



$range = $_GET['range'];
if(''==$range) $range = 'week';
$start = $_GET['start'];
if(''==$start) $start = date( 'Y-m-d', strtotime('last Monday') );
$daybreak = $_GET['daybreak'];
if(''==$daybreak) $daybreak = '07:00';

include_once ($offroot.'class.printToBrowser.php');
$printToBrowser = new printToBrowser;
try {
switch ($range) {
	case 'index':
		$photoindex = $user->getPhotoIndex();
		$printToBrowser->outputPhotoIndex($user,$photoindex);
		break;
	case 'day':
		$photostream = $user->getPhotostream($start,$daybreak,$range);
		$printToBrowser->outputDayStream($user,$photostream);
		break;
	case 'week':
		$photostream = $user->getPhotostream($start,$daybreak,$range);
		$printToBrowser->outputWeekStream($user,$photostream);
		break;
}
} // try
catch (flickrUserNotFoundException $ex) {
	//echo '<pre>';
	$message = $ex->getMessage();
	//echo 'Flickr said User '.$message;
	//echo '</pre>';

	header('Location: http://'.$indexURL.'?page=flickr&username='.$username.'&error='.$message.'#error');
	die();
	//$printToBrowser->outputException($ex);
}
catch (flickrPhotosetNotFoundException $ex) {
	//echo '<pre>';
	$message = $ex->getMessage();
	//echo 'Flickr capture photoset '.$message;
	//echo '</pre>';
	header('Location: http://'.$indexURL.'?page=flickr&error='.$message.'#error');
	die();
	//$printToBrowser->outputException($ex);
}
catch (flickrException $ex) {
	//echo '<pre>';
	$message = $ex->getMessage();
	//echo 'Flickr capture '.$message;
	//echo '</pre>';
	header('Location: http://'.$errorURL.'?type=fatal&error='.$message);
	die();
	//$printToBrowser->outputException($ex);
} catch (Exception $ex) {
	$message = $ex->getMessage();
	//echo $message;
	header('Location: http://'.$errorURL.'?error='.$message);
	die();
}

?>
