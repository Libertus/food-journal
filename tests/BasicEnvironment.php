<?php
require_once 'PHPUnit/Framework.php';

/**
 * Basic environmental tests for the application
 */
class BasicEnvironment extends PHPUnit_Framework_TestCase
{
    const MINIMUM_ZEND_VERSION = '1.6.0';
    
    /**
     * Is Zend Framework available without any include jiggery-pokery?
     */
    public function testZendFrameworkAvailable()
    {
        $this->assertTrue( class_exists( 'Zend_Version' ) );
    }

    /**
     * Is the available version of Zend Framework compatible with the application?
     */
    public function testZendFrameworkVersionIsCompatible()
    {
        $this->assertTrue(
            Zend_Version::compareVersion( self::MINIMUM_ZEND_VERSION ) <= 0,
            'Need at least Zend Framework ' . self::MINIMUM_ZEND_VERSION
            . ', have ' . Zend_Version::VERSION
        );
    }
}
