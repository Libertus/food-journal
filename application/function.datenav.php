<?php
function getDayNavigation ($thisday,$photodays) {
	
	$key = array_search($thisday,$photodays) ;
	if (0 === $key) {
		$nearby = array_slice($photodays,0,2);
		$nav['thisday'] = $nearby[0];
		$nav['prevday'] = $nearby[1];
		$nav['thisweek'] = getStartofWeek($thisday,$photodays);
	} else {
		if($key) {
			$nearby = array_slice($photodays,( $key - 1 ), 3);
			$nav['nextday'] = $nearby[0];
			$nav['thisday'] = $nearby[1];
			$nav['prevday'] = $nearby[2];
			$nav['showweek'] = getStartofWeek($thisday,$photodays);
		}
	}
	return $nav;
}
function getWeekNavigation ($thisweek,$photodays) {
	// just got to add and subtract 7 days 
	$thisday = strtotime($thisweek);
	$nav['prevweek'] = date('Y-m-d',$thisday - (7 * 24 * 60 * 60));

	$nav['thisweek'] = $thisweek;
	$nav['startweek'] = date('l',strtotime($thisweek));

	// dont show next week in the future
	$nextweek = $thisday + (7 * 24 * 60 * 60);
	$now = strtotime('now');
	if ($nextweek < $now) {
		$nav['nextweek'] = date('Y-m-d',$nextweek);
	}
	return $nav;
}
function getStartofWeek($thisday,$photodays,$weekstarts='Monday') {
	$date_array = getdate(strtotime($thisday));

	$wday = -1 * $date_array['wday'];
	if('Monday'==$weekstarts) $wday++;
	$date = new DateTime($thisday);
	$date->modify($wday.' day');
	$startweek = (string)$date->format('Y-m-d');
	return $startweek;
}
?>