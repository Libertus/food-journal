<?php
class User {
	public $username;
	public $displayname;
	public $provider;
	public $providerdetails;

	public function __construct( $username, $displayname, $provider, $providerdetails ) {
		$this->username = $username;
		$this->displayname = $displayname;
		$this->provider = $provider;
		$this->providerdetails = $providerdetails;

	}

	public static function getNamed( $name ) {
		$all = self::getAll();
		return $all[$name];
	}

	public function getGuest( $name , $provider ) {
		return new User($name,'',$provider,'');
	}


	public function getPhotostream ($date_start='',$daybreak='00:00',$range='') {
		include 'factory.php';

		$provider = createProvider ($this) ;
		$provider->getProviderDetails($this);
		$photostream = $provider->getPhotostream($this);

		include 'function.photostream.php';
		$filtered = filterPhotostream($photostream,$date_start,$daybreak,$range);

		$filtered['provider'] = $details = $this->getProviderDetails();
		$this->setProviderDetails($details);
		//echo '<pre>';print_r($details);echo '</pre>';
		if( empty($this->displayname) ) {
			$this->setDisplayname($details['displayname']);
		}
		$filtered['user'] = (array)$this;

		return $filtered;
	}

	public function getPhotoIndex() {
		include 'factory.php';
		$provider = createProvider ($this) ;
		$provider->getProviderDetails($this);
		$photostream = $provider->getPhotostream($this);

		include 'function.photostream.php';
		$return['photos'] = photoNavigation($photostream);
		$return['provider'] = $details = $this->getProviderDetails();
		$this->setProviderDetails($details);
		if( empty($this->displayname) ) {
			$this->setDisplayname($details['displayname']);
		}
		$return['user'] = (array)$this;

		return $return;
	}

	public function getProvider() {
		return (string)$this->provider;
	}
	public function getUsername() {
		return (string)$this->username;
	}
	public function getDisplayname() {
		return (string)$this->displayname;
	}
	public function setDisplayname($n) {
		$this->displayname = (string)$n;
	}
	public function setProviderDetails($providerdetails) {
		$this->providerdetails = $providerdetails;
	}
	public function getProviderDetails() {
		return $this->providerdetails;
	}
	public function listUsers() {
		$all = self::getAll();
		foreach ($all as $username => $user) {
			$users[$user->getUsername()]['provider'] = $user->getProvider();
			$users[$user->getUsername()]['displayname'] = $user->getDisplayname();
		}
		return $users;
	}

	public static function getAll()
	{

		static $allUsers = null;
		if( $allUsers == null ) $allUsers =
		array(
			'vege foodie'
			=> new User(
				'vege foodie'
				,'Vege Foodie'
				,'flickr'
				,array(
					'user' => array(
						'displayname'=>'G.A.Stroveg',
						'photoset_id'=>'72157606134138202',
						'photosurl'=> 'http://www.flickr.com/photos/vegefoodie/',
						'userid'=>'28051645@N03'
					)
				)
			),

			'CK in TX'
			=> new User(
				'CK in TX'
				,'Charles Kim'
				,'flickr'
				,array(
					'user' => array(
						'photoset_id' => '72157600357834922',
						'userid' => '40046624@N00',
						'displayname' => 'Charles Kimson',
						'username' => 'CK in TX',
						'photosurl' => 'http://www.flickr.com/photos/ventoetc/',
						'profileurl' => 'http://www.flickr.com/people/ventoetc/'
					)
				)
			),
			'espanol' => new User('espanol','Aaron Vincent','flickr','',''),
			'libertus96' => new User( 'libertus96', 'Paul M', 'picasa', '' ),
			'jodimichelle' => new User('jodimichelle','Jodi Schaap','flickr','','')
		);
		return $allUsers;
	}
}
?>