<?php 
/* smarty function for food journal */

function smarty_function_periodOfDay ($params,&$smarty) {
		$time = $params['time'];
		if (substr($time,0,2) > 18 ) {
			$period = 'eve';
		} elseif (substr($time,0,2) > 12 ){
			$period = 'pm';
		} else {
			$period = 'am';
		}
		return $period;
}
?>