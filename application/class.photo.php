<?php
class Photo {
	public $timestamptaken;
	public $thumbnail;
	public $picture;
	public $title;
	public $link;

	public function __construct( $takenUTC, $thumbnail, $picture, $title, $link='' ) {
		$this->timestamptaken = $takenUTC;
		$this->thumbnail = $thumbnail;
		$this->picture = $picture;
		$this->title = $title;
		$this->link = $link;
	}

	public function getTimestampTaken() {
		return $this->timestamptaken;
	}
	public function getDateTaken() {
		return gmdate('Y-m-d', $this->timestamptaken);
	}
	public function getHourTaken() {
		return gmdate('H:00', $this->timestamptaken);
	}
}
?>