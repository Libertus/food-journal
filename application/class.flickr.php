<?php

class flickrException extends Exception {}
class flickrUserNotFoundException extends flickrException {}
class flickrPhotosetNotFoundException extends flickrException {}
class flickrSystemException extends flickrException {}

class flickr {
	//const api_key = '0ab25173f2f8111cd9e056fc9ce2e444'; // testing invalid api key
	const api_key = '0aa25173f2f8111cd9e056fc9ce2e444';
	const seconds_in_day = 86400;

	public function __toString() {
		return 'Flickr';
	}

	public function isThere() {
		$params = array();
		// if this call fails an exception is thrown
		$results = $this->callFlickrAPI('flickr.test.echo',$params);
		return true;
	}


	// gather user information
	public function getProviderDetails (User $user) {
		$providerdetails['userid'] = $userid = $this->getUserID($user);
		$providerdetails['username'] = $user->getUsername($user);
		$providerdetails['person'] = $person = $this->getUserDetails($userid);
		$providerdetails['displayname'] = $person['realname']['_content'];
		$providerdetails['link'] = $person['photosurl']['_content'];
		$providerdetails['photoset'] = $this->getPhotosetID($userid);
		$providerdetails['provider'] = 'Flickr';
		$providerdetails['sourcephotos'] = 'http://www.flickr.com/photos/'.$userid.'/sets/'.$providerdetails['photoset'];
		$user->setProviderDetails($providerdetails);
	}
	private function getUserID ($user) {
		$params = array('username'=>$user->getUsername());
		$results =  $this->callFlickrAPI ('flickr.people.findByUsername', $params);
		return $results['user']['nsid'];
	}
	private function getUserDetails ($userid) {
		$params = array("user_id"=>$userid);
		//$user_details = $this->callFlickrAPI("flickr.peopl.getInfo",$params); // testing invalid method call
		$user_details = $this->callFlickrAPI("flickr.people.getInfo",$params);
		return $user_details['person'];
	}
	private function getPhotosetID($userid) {
		$params = array("user_id"=>$userid);
		$results = $this->callFlickrAPI("flickr.photosets.getList",$params);
		$photosets = $results['photosets']['photoset'];
		$count = count($photosets);
		for ( $i = 0; $i < $count ; $i++ ) {
			if( strcasecmp($photosets[$i]['title']['_content'],'food journal') == 0 ) {
				return $photosets[$i]['id'];
			}
		}
	}


	public function getPhotostream ($user) {
		$providerdetails = $user->getProviderDetails();
		include_once 'class.photo.php';

		$params = array('user_id'=>$providerdetails['userid'],
						'photoset_id'=>$providerdetails['photoset'],
						'extras'=>'date_taken');
		$return = $this->callFlickrAPI("flickr.photosets.getPhotos",$params);

		$photos = $return['photoset']['photo'];

		//echo "count of photos ".count($photos).'<br/>';

		// sanity check the sort is in reverse order according to date_taken
		foreach ($photos as $key => $row) {
    			$date_taken[$key]  = $row['datetaken'];
		}
		array_multisort($date_taken, SORT_ASC, $photos);

		// add the img src url & enhance the title
		//thumbnail url http://farm{farm-id}.static.flickr.com/{server-id}/{id}_{secret}_[mstb].jpg
		//link url http://www.flickr.com/photos/{user-id}/{photo-id}
		foreach ($photos as $key => $photo) {
			$thumbnail = "http://farm".$photo['farm'].".static.flickr.com/".$photo['server']."/".$photo['id']."_".$photo['secret']."_t.jpg";
			$picture = "http://farm".$photo['farm'].".static.flickr.com/".$photo['server']."/".$photo['id']."_".$photo['secret']."_m.jpg";
			$title = substr($photo['datetaken'],11,5).' '.$photo['title'];
			$takenUTC = strtotime($photo['datetaken'].'Z');
			$link = 'http://www.flickr.com/photos/'.$providerdetails['userid'].'/'.$photo['id'];
			$photostream[] = new Photo($takenUTC, $thumbnail, $picture, $title, $link);
		}

		return $photostream;
	}

	private function callFlickrAPI ( $api, array $params ) {
		
		$base_url = "http://api.flickr.com/services/rest/?";
		$params['method'] = $api;
		$params['api_key'] = self::api_key;
		$params['format'] = "php_serial";
		$api_url = $base_url . http_build_query($params);

		$results = @ unserialize(file_get_contents($api_url));
		
		//$results =''; //testing exceptions
		if(!is_array($results) ) {
			throw new flickrException('We had a problem talking to Flickr, wait a little and try again');
		}
		//$results = array(); //testing exceptions
		if (!array_key_exists('stat',$results)) { //sanity check there is a status
			throw new flickrException ('The Flickr message was scrambled. Please let us know and we will look into it');
		}
		//$results['stat'] = 'bad';
		if('ok' == $results['stat']) {
			//echo '<br>Flickr api call : '.$api;
			//var_dump($results);
			return $results;
		} else {
			// find out what type of call was being made and throw appropriate exception
			$a = explode('.',$api);
			$type = $a[1];
			
			//echo '<br>Flickr api call : '.$api. ' type '.$type;
			//var_dump($results);
			if(1==$results['code']) {
				switch ($type) {
				case 'people':
					throw new flickrUserNotFoundException ( $results['message']);
					break;
				case 'photosets':
					throw new flickrPhotosetNotFoundException ( $results['message'] );
					break;
				default :
					throw new flickrException ( $results['message']);
				}
			} elseif (100<$results['code']){
				throw new flickrSystemException ( $results['message']);			
			} else {
				throw new flickrException ( $results['message']);	
			//var_dump($results);
			}
		}
	}
}
?>