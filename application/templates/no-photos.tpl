{if 0 eq $navigation.this.count}
<div id="no-photos">
<h3>No photos uploaded for this {$navigation.range}</h3>
<ul>
{if $navigation.next.start}
<li>Nearest later {$navigation.range} with photos is <a href='?username={$user.username|escape:'url'}&amp;provider={$user.provider}&amp;range={$navigation.range}&amp;start={$navigation.next.start|date_format:"%Y-%m-%d"}&amp;daybreak={$navigation.daybreak}'>{$navigation.next.start|date_format:"%e %B %Y"}</a></li>
{/if}
{if $navigation.prev.start}
<li>Nearest earlier {$navigation.range} with photos is <a href='?username={$user.username|escape:'url'}&amp;provider={$user.provider}&amp;range={$navigation.range}&amp;start={$navigation.prev.start|date_format:"%Y-%m-%d"}&amp;daybreak={$navigation.daybreak}'>{$navigation.prev.start|date_format:"%e %B %Y"}</a></li>
{/if}
</ul>
</div>
{/if}