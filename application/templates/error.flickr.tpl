<div id="error">
{if 'User not found' eq $error}
<h3>Flickr said user "{$username}" not found</h3>
<p>Please use your screen name.</p>
<p>You will find this on your Flickr account page or at the top of every page after "Signed in as.."</p>
{else}
<h3>Flickr cannot find set called "food journal"</h3>
<p>Please check you have created a set for your food photos and called it "food journal"</p>
{/if}
</div>