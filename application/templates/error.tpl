{include file="header.tpl"}
<div id="error">
<h3>Something has gone {if 'fatal' eq $type}badly {/if}wrong</h3>
<p>Message : <span class='error'>{$error|escape}</span></p>
{if 'fatal' eq $type}
<p>Please <a href='mailto:foodjournal@made-accessible.com?subject=something went wrong&body=the message was : {$error|escape:'url'}'>email us the message above</a> so we can fix this. Thanks!</p>
{/if}
</div>
</div><!-- end main -->

