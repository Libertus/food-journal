{* wide weekstream x-axis = hour, y-axis = day *}
{* photostream array delivered day->hour->photo *}
{include file="header.tpl" displayname=$user.displayname date=$navigation.this.start}
<div id="wide-weekstream">
<h2>Week beginning {$navigation.this.start|date_format:"%e %B %Y"} </h2>
<table>
<thead>
<tr>
<th>Days/hours</th>
{foreach from=$hourhdrs item=hdr}
{assign var='period' value='am'}
{if $hdr gt '11:59'}
{assign var='period' value='pm'}
{/if}
{if $hdr gt '17:59'}
{assign var='period' value='eve'}
{/if}
<th class="{$period}">
{$hdr|date_format:"%l%p"}
</th>
{/foreach}
</tr>
</thead>
<tbody>
{foreach from=$photostream key=date item=days}
<tr>
<th class="weekday"><a href="?username={$user.username}&amp;range=day&amp;start={$date|date_format:'%Y-%m-%d'}&amp;daybreak={$navigation.daybreak}">{$date|date_format:"%A"}<br/>{$date|date_format:"%e %B %Y"}</a></th>
{foreach from=$days key=hour item=photos}
{assign var='period' value='am'}
{if $hour gt '11:59'}
{assign var='period' value='pm'}
{/if}
{if $hour gt '17:59'}
{assign var='period' value='eve'}
{/if}
<td class="{$period}">	

{if count($photos)}
<ul>
{foreach from=$photos item=photo}
<li><img src="{$photo->thumbnail}" alt="{$photo->title}" title="{$photo->title}" /></li>
{/foreach}
</ul>
{/if}

</td>
{/foreach}
</tr>
{/foreach}
</tbody>
</table>
</div>
{include file="navigation.tpl" navigation=$navigation user=$user range='week'}
{include file="footer.tpl" userlist=$userlist}