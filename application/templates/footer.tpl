<div id="footer">
<div id="other-journals">
<h2>Other Photo Food Journals</h2>
<ul>
{foreach from=$userlist key=user item=details}
<li><a href="foodjournal.php?username={$user|escape:'url'}&amp;range=week&amp;provider={$details.provider}">{$details.displayname|escape}</a></li>
{/foreach}
</ul>
</div>
<div id="about-photo-food-journal">
<h2>About Photo Food Journal</h2>
<ul>
<li><a href="index.php">Photo Food Journal home</a></li>
<li><a href="index.php?page=about">About Photo Food Journal</a></li>
<li><a href="index.php?page=news">News and research</a></li>
<li><a href="index.php?page=flickr">Using Flickr</a></li>
<li><a href="index.php?page=picasa">Using Picasa</a></li>

</ul>
</div>
</div>
</div><!-- end main -->
</body>
</html>