{include file="header.tpl" displayname="News and research from "}
<div id="introduction">
<h2>5 September 2008 : News Scientist reports <q>Snap-happy dieters reap benefits</q></h2>
<blockquote><p><q>WATCHING what you eat really does help, at least if you do it through a camera lens. That's the conclusion of a study of dieters' eating habits comparing the effect of written food diaries with taking a snapshot of each meal.</q></p></blockquote>
<p>We've been taking pictures of our meals for a couple of months and know how true this is. They most definitely <q>provide powerful visual documentation of snack binges</q> and because they are shared in public we have found they make us think before we eat. <a href="http://www.newscientist.com/channel/health/mg19926725.300-snaphappy-dieters-reap-benefits.html">Read the New Scientist article</a>. </p>
</div>
{include file="footer.tpl"}