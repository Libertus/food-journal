{include file="header.tpl" displayname="Using Flickr with "}
<div id='introduction'>
<h2>Prepare your Flickr account</h2>
<h3>Note down your Flickr screen name</h3>
<p>When you sign into Flickr your screen name is at the top after "signed in as". It's also on the personal information tab of your account. If you don't yet have a Flickr account, then what's stopping you? <a href="http://flickr.com">Set up a Flickr account</a></p>
<h3>Create a set on Flickr called "foodjournal"</h3>
<p>Sets are an easy way to organise your photos on Flickr. If you have some "what I ate" pictures already then add them to the set</p>
<h2>Take some pictures</h2>
<h3>Take pictures of everything you eat today and upload them to Flickr</h3>
<p>Use your mobile (cell) phone or a digital camera to snap everything you eat. Upload them any way you want; email from your phone, from your computer or on the Flickr web site.</p>
<h3>Give them titles and add to your "foodjournal" set</h3>
<p>Give each picture a title e.g. "beans on toast", "jelly and icecream". Add them to your "foodjournal" set.</p>
<h2>Use Photo Food Journal to see what you ate when</h2>
{if $error}
{include file="error.flickr.tpl" error=$error username=$username}
{/if}
<form action="foodjournal.php" method="get">
<fieldset>
<label for="username">Enter your Flickr screen name</label>
<p><input type="text" name="username" id="username"/>
<input type="submit" name="submit" value="see your food journal"/>
<input type="hidden" name="provider" value="flickr"/></p>
</fieldset>
</form>
<h3>Add your name to the rollcall</h3>
<p>There's a list of existing Photo Food Journals at the bottom of the page. <a href='mailto:foodjournal@made-accessible.com'>Email us</a> with your Flickr screen name to be added to the list.</p>
<h2>FAQ</h2>
<p><a href='mailto:foodjournal@made-accessible.com'>Please ask</a> and we will answer!</p>
</div>
{include file="footer.tpl"}
