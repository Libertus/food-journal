{include file="header.tpl" title="All entries" displayname=$user.displayname}
<div id='introduction'>
<h2>All entries</h2>
{foreach from=$photoindex key=year item=weeks}
<h3>{$year}</h3>
<ul>
{foreach from=$weeks key=weekno item=week}
<li><a href="?username={$user.username}&amp;provider={$user.provider}&amp;range=week&amp;start={$week.monday|date_format:'%Y-%m-%d'}">{$week.monday|date_format:"%e %B %Y"} ({$week.count})</a></li>
{/foreach}
</ul>
{/foreach}
</div>
{include file="footer.tpl"}