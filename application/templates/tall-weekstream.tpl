{* tall weekstream x-axis = day, y-axis = hour *}
{* photostream array delivered hour->day->photo *}
{include file="header.tpl" displayname=$user.displayname date=$navigation.this.start}
<div id="tall-weekstream">
<h2>Week beginning {$navigation.this.start|date_format:"%e %B %Y"}</h2>
{include file="no-photos.tpl" navigation=$navigation}
<table>
<thead>
<tr>
<th>Hours/days</th>
{foreach from=$dayhdrs item=date}
<th class="weekday"><a href="?username={$user.username|escape:'url'}&amp;provider={$user.provider|escape:'url'}&amp;range=day&amp;start={$date|date_format:'%Y-%m-%d'}&amp;daybreak={$navigation.daybreak}">{$date|date_format:"%A"}<br/>{$date|date_format:"%e %B %Y"}</a></th>
{/foreach}
</tr>
</thead>
<tbody>
{foreach from=$photostream key=hour item=days}
<tr>
{assign var='period' value='am'}
{if $hour gt '11:59'}
{assign var='period' value='pm'}
{/if}
{if $hour gt '17:59'}
{assign var='period' value='eve'}
{/if}
<th class="{$period}">
{$hour|date_format:"%l%p"}
</th>

{foreach from=$days key=day item=photos}
<td class="{$period}">	

{if count($photos)}
<ul>
{foreach from=$photos item=photo}
<li><img src="{$photo->thumbnail}" alt="{$photo->title|escape}" title="{$photo->title|escape}" /></li>
{/foreach}
</ul>
{/if}

</td>
{/foreach}
</tr>
{/foreach}
</tbody>
</table>
</div>
{include file="navigation.tpl" navigation=$navigation user=$user range='week'}
{include file="footer.tpl" userlist=$userlist}