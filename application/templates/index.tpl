{include file="header.tpl"}
<div id="introduction">
<h2>A fun and easy way to see what you eat and when - <em>and now supported by University research!</em></h2>

<p>Take a picture of everything you eat (before you eat it!) and upload it to Flickr or Picasa. Then come along to the Photo Food Journal and see what you ate and when.</p>

<h2>5 September 2008 : New Scientist reports that photo food journals are the best for dieters</h2>
<blockquote><p><q>WATCHING what you eat really does help, at least if you do it through a camera lens. That's the conclusion of a study of dieters' eating habits comparing the effect of written food diaries with taking a snapshot of each meal.</q></p></blockquote>
<p>The <a href="http://www3.interscience.wiley.com/journal/121389729/abstract?CRETRY=1&amp;SRETRY=0" title="abstract available for free">research by Lydia Zepeda and David Deal</a> at the University of Wisconsin-Madison found the food journals provided <q>powerful visual documentation of snack binges</q> and made people think about what they ate - before they ate it. <a href="http://www.newscientist.com/channel/health/mg19926725.300-snaphappy-dieters-reap-benefits.html">Read the New Scientist article</a> and then set up your food journal! </p>

<h2>Try it out today</h2>
<ul>
<li><a href="index.php?page=flickr">with Flickr</a></li>
<li><a href="index.php?page=picasa">with Picasa</a></li>
</ul>

</div>
{include file="footer.tpl" userlist=$userlist}
