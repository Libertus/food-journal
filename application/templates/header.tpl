<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="application/xhtml+xml; charset=UTF-8"/>
{if $title}
<title>{$title|escape} : {$displayname|escape} Photo Food Journal</title>
{elseif $date}
<title>{$date|date_format:"%e %B %Y"} : {$displayname|escape} : Photo Food Journal</title>
{else}
<title>{$displayname|escape} Photo Food Journal</title>
{/if}
<link rel="stylesheet" type="text/css" href="style.css"/>
</head>
<body>
<div id='main'>
<h1>{$displayname|escape} Photo Food Journal</h1>
