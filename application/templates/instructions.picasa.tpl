{include file="header.tpl" displayname="Using Picasa with "}
<div id='introduction'>
<h2>Create Picasa account for your food pictures</h2>
<p>If you have an account already for general pictures then please create a new one just for your food journal.</p>
<h2>Take some pictures</h2>
<h3>Take pictures of everything you eat today and import them to Picasa</h3>
<p>Use your mobile (cell) phone or a digital camera to snap everything you eat. Import them into Picasa.</p>
<h3>Give them titles and upload them</h3>
<p>Give each picture a title e.g. "beans on toast", "jelly and icecream". Upload them to your Web Album.</p>
<h2>Use Photo Food Journal to see what you ate when</h2>
{if $error}
{include file="error.picasa.tpl" error=$error username=$username}
{/if}
<form action="foodjournal.php" method="get">
<fieldset>
<label for="username">Enter your Picasa Web Albums Public Gallery URL</label>
<p><input type="text" name="username" id="username"/>
<input type="submit" name="submit" value="see your food journal"/>
<input type="hidden" name="provider" value="picasa"/></p>
</fieldset>
</form>
<p><strong>Please note :</strong> This may take a little time to load first time round, particularly if you have a lot of photographs in your album. There's a technical reason for this but we are working on a way to make it quicker.</p>
<h3>Add your name to the rollcall</h3>
<p>There's a list of existing Photo Food Journals at the bottom of the page. <a href='mailto:foodjournal@made-accessible.com'>Email us</a> with your Picasa Public Gallery URL to be added to the list.</p>
<h2>FAQ</h2>
<p><a href='mailto:foodjournal@made-accessible.com'>Please ask</a> and we will answer!</p>
</div>
{include file="footer.tpl"}
