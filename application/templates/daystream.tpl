{include file="header.tpl" displayname=$user.displayname date=$navigation.this.start}
<div id="daystream">
<h2>{$navigation.this.start|date_format:"%A, %e %B %Y"}</h2>
{include file="no-photos.tpl" navigation=$navigation}
{foreach from=$photostream key=date item=days}
{foreach from=$days key=hour item=photos}
{assign var='period' value='am'}
{if $hour gt '11:59'}
{assign var='period' value='pm'}
{/if}
{if $hour gt '17:59'}
{assign var='period' value='eve'}
{/if}
<div class="{$period}">
<h3>{$hour|date_format:"%l%p"}</h3>
{if count($photos)}
<ul>
{foreach from=$photos item=photo}
<li><img src="{$photo->picture}" alt="" /><span>{$photo->title|escape}</span></li>
{/foreach}
</ul>

{/if}
<p class="clear"/>
</div>
{/foreach}
{/foreach}
</div>

{include file="navigation.tpl" navigation=$navigation user=$user range='day'}
{include file="footer.tpl" userlist=$userlist}