<div id="change-daybreak" class="navigation">
<form action="" method="get">
<h2>Change start of day</h2>
<fieldset>{html_options name="daybreak" options=$daybreakoptions selected=$daybreaksel}
<input type="hidden" name="provider" value="{$user.provider|escape}"/>
<input type="hidden" name="username" value="{$user.username|escape}"/>
<input type="hidden" name="range" value="{$navigation.range}"/>
<input type="hidden" name="start" value="{$navigation.this.start|date_format:'%Y-%m-%d'}"/>
<input type="submit" name="submit" value="set daybreak"/>
</fieldset>
</form>
</div>

<div id="change-range" class="navigation">
<h2>Browse a different {$range}</h2>
<ul>
{if $navigation.prev.start}
<li><a href='?username={$user.username|escape:'url'}&amp;provider={$user.provider}&amp;range={$navigation.range}&amp;start={$navigation.prev.start|date_format:"%Y-%m-%d"}&amp;daybreak={$navigation.daybreak}' title='{$navigation.prev.start|date_format:"%d %B %Y"}'>Prev {$navigation.range}</a></li>
{/if}
{if $navigation.next.start}
<li><a href='?username={$user.username|escape:'url'}&amp;provider={$user.provider}&amp;range={$navigation.range}&amp;start={$navigation.next.start|date_format:"%Y-%m-%d"}&amp;daybreak={$navigation.daybreak}' title='{$navigation.next.start|date_format:"%d %B %Y"}'>Next {$navigation.range}</a></li>
{/if}
</ul>
<p>Note: links go to nearest next and previous {$range}s with photos. Empty {$range}s are skipped.</p>
{if $navigation.this.week}
<h2>Browse the week</h2>
<p><a href='?username={$user.username|escape:'url'}&amp;provider={$user.provider}&amp;range=week&amp;start={$navigation.this.week|date_format:"%Y-%m-%d"}&amp;daybreak={$navigation.daybreak}'>Week starting {$navigation.this.week|date_format:"%e %B %Y"}</a></p>
{/if}
</div>

<div id="explore-user" class="navigation">
<h2>More about {$user.displayname|escape}</h2>
<ul>
<li><a href="?username={$user.username|escape:'url'}&amp;provider={$user.provider|escape:'url'}&amp;range=index">All Photo Food Journal entries</a></li>
<li><a href='{$provider.sourcephotos}'>Original photos on {$provider.provider|escape}</a></li>
</ul>
</div>