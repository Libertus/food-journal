{include file="header.tpl" displayname="About the "}
<div id='introduction'>
<h2>Our challenge : to make a really easy and accessible food journal</h2>
<p>As anyone who has been on a diet will tell you, knowing what you eat is the first step to making healthier choices. The second is sharing it with supportive friends.</p>
<h3>A pen and paper is all you need</h3>
<p>Paper food journals have been around since diets began. They are simple and effective. All you need is a pen and paper - and a pinch of discipline. But <a href='index.php?page=news'>research has found</a> that people often don't fill in their paper food journals until the end of the day. Sometimes they forget some things they ate or how much they ate. </p>
<h3>Seeing what you eat is more powerful</h3>
<p> What ever you think of them, the change-your-life TV shows have a powerful dietary tool. They all show a table laden with all the food the poor chump eats in a week. Suddenly the number of bags of crisps they eat in a week, or glass of wine becomes very real while we viewers think how <em>our table</em> would be healthier or lighter or actually much the same.</p>
<p>Gathering all your foods for the week for one photograph is tricky for most of us. Taking pictures of your food as you go alone is much easier. It shows quantity and quality of food better than words alone and gathered together your pictures build a virtual <em>food table for every week</em>.</p>
<h3>Photos make you think about what you eat - before you eat it</h3>
<p>Taking a picture before you eat takes only a moment. But its enough of a moment just to think whether you are really hungry or are you just eating that last flapjack to <em>be tidy</em> ? If you share your food pictures that moment might also be the time when you think about what your friends will say about your third bowl of icecream, or the chocolate you are supposed to be sworn off.</p>
<h3>Photos in a timeline are even more informative</h3>
<p>Suddenly the bag of crisps and can of cola to <em>get you through</em> the afternoon becomes obvious. Or that on days you skip breakfast you eat more at lunchtime. Written journals just don't compete in the ways you can display and reflect on what you eat.</p>
<h2>Our design considerations</h2>
<h3>Mobile phones are ubiquitous</h3>
<p>The majority of phones (over say &#163;100) have a camera included. Most people keep their phones with them all the time. In fact they are probably more likely at any time to have their mobile on them than a pen and paper.</p>
<p>So taking pictures of what you eat is something you can do with what you already have on you.</p>
<h3>Online photo libraries provide easy upload</h3>
<p>Flickr offers the widest range by far of ways you can get pictures online. Members can email direct from their phones, upload from a program on their computer or use the Flickr web site. Picasa web albums are integrated with Google's Picasa and you can upload direct on the web site. Neither service is complete nor totally accessible (in a web sense) but choice of access is important here.</p>
<h3>Using it should be simple</h3>
<p>It is. No sign up or password required. Just enter your user name or URL on the appropriate page (<a href='index.php?page=flickr#username'>Flickr</a> or <a href='index.php?page=picasa#username'>Picasa</a>) and see your journal.</p>
<h3>Browsing it should be simple and accessible and informative</h3>
<p>Photo Food Journals are presented in simple web pages day by day or week by week. Single days show big clear pictures and descriptions of meals. Weeks show patterns of when you eat without losing descriptions (in the alt text that pops up when you hover on a picture).</p>
<p>Moving around a food journal is simple one step forwards or backwards so you never get lost or overwhelmed. A full list of entries is listed on a separate page. We have used 12 hour notation and words for months so everyone can understand.</p>
<p>And every page links back to the original photos so you can explore more about the journal author.</p>
<h2>The result</h2>
<p>A very simple food journal displayed as a timeline so you can see what you eat and when.</p>
<p><a href='mailto:foodjournal@made-accessible.com'>Please email with any comments</a>. Include your user name if you are willing to be included in the foot notes.</p>
</div>
{include file="footer.tpl"}