<?php

class printToBrowser {


	public function outputDayStream ($user,$photostream,$start_of_day = '00:00') {
	/*
	*
	* The data is displayed by smarty template
	*
	* @params array,string,string
	* @return -
	**/
		include_once 'smarty/Smarty.class.php';
		$smarty = new Smarty();
		include_once 'smarty/config.php';
		$smarty->assign('user',$photostream['user']);
		$smarty->assign('provider',$photostream['provider']);
		$smarty->assign('navigation',$photostream['nav']);
		$smarty->assign('photostream',$photostream['photos']);

		// date being displayed
		$dates = array_keys($photostream['photos']);
		$date = $dates[0];
		$smarty->assign('date',$date);

		// provide day break navigation
		$smarty->assign('daybreakoptions',$photostream['nav']['daybreaknav']);
		$smarty->assign('daybreaksel',$photostream['nav']['daybreak']);

		// user list for footer
		$userlist = $user->listUsers();
		$smarty->assign('userlist',$userlist);

		$smarty->display('daystream.tpl');
	}



	public function outputWeekStream ($user,$photostream) {
	/*
	* This function receives 7 days of photo data.
	* The data is displayed by smarty template
	*
	* @params array,string,string
	* @return -
	**/
		include_once 'smarty/Smarty.class.php';
		$smarty = new Smarty();
		include_once 'smarty/config.php';
		$smarty->assign('user',$photostream['user']);
		$smarty->assign('provider',$photostream['provider']);
		$smarty->assign('navigation',$photostream['nav']);
		$smarty->assign('photostream',$photostream['photos']);

		// week beginning date is first day in stream
		$dates = array_keys ($photostream['photos']);
		$weekstarts = $dates[0];
		$smarty->assign('week',$weekstarts);

		// provide day break navigation
		$smarty->assign('daybreakoptions',$photostream['nav']['daybreaknav']);
		$smarty->assign('daybreaksel',$photostream['nav']['daybreak']);

		// user list for footer
		$userlist = $user->listUsers();
		$smarty->assign('userlist',$userlist);

		/*if('hour'==$x) {
			// hour headings
			$days = array_keys($photostream['photos']);
			$hours = array_keys ($photostream['photos'][$days[0]]);
			$smarty->assign('hourhdrs',$hours);

			$smarty->display('wide-weekstream.tpl');
		} else {*/
			// day headings
			$hours = array_keys($photostream['photos']);
			$days = array_keys ($photostream['photos'][$hours[0]]);
			$smarty->assign('dayhdrs',$days);

			$smarty->display('tall-weekstream.tpl');
		//}
	}



	public function outputPhotoIndex($user,$photoindex) {
		include_once 'smarty/Smarty.class.php';
		$smarty = new Smarty();
		include_once 'smarty/config.php';
		$smarty->assign('photoindex',$photoindex['photos']);
		$smarty->assign('user',$photoindex['user']);
		// user list for footer
		$userlist = $user->listUsers();
		$smarty->assign('userlist',$userlist);

		$smarty->display('userindex.tpl');
	}

	public function printHomepage() {
		include_once 'smarty/Smarty.class.php';
		$smarty = new Smarty();
		include_once 'smarty/config.php';
		include_once 'class.user.php';
		$userlist = User::listUsers();
		$smarty->assign('userlist',$userlist);
		$smarty->display('index.tpl');
	}
	public function printAbout() {
		include_once 'smarty/Smarty.class.php';
		$smarty = new Smarty();
		include_once 'smarty/config.php';
		include_once 'class.user.php';
		$userlist = User::listUsers();
		$smarty->assign('userlist',$userlist);		
		$smarty->display('about.tpl');	
	}	
	public function printNews() {
		include_once 'smarty/Smarty.class.php';
		$smarty = new Smarty();
		include_once 'smarty/config.php';
		include_once 'class.user.php';
		$userlist = User::listUsers();
		$smarty->assign('userlist',$userlist);		
		$smarty->display('news.tpl');	
	}		
	public function printFlickr($error='',$username='') {
		include_once 'smarty/Smarty.class.php';
		$smarty = new Smarty();
		include_once 'smarty/config.php';
		include_once 'class.user.php';
		$userlist = User::listUsers();
		$smarty->assign('error',$error);
		$smarty->assign('username',$username);
		$smarty->assign('userlist',$userlist);
		$smarty->display('instructions.flickr.tpl');	
	}
	public function printPicasa() {
		include_once 'smarty/Smarty.class.php';
		$smarty = new Smarty();
		include_once 'smarty/config.php';
		include_once 'class.user.php';
		$userlist = User::listUsers();
		//$smarty->assign('error',$error);
		$smarty->assign('username',$username);
		$smarty->assign('userlist',$userlist);
		$smarty->display('instructions.picasa.tpl');	
	}
	public function printErrorpage($error,$type) {
		include_once 'smarty/Smarty.class.php';
		$smarty = new Smarty();
		include_once 'smarty/config.php';
		$smarty->assign('error',$error);
		$smarty->assign('type',$type);
		$smarty->display('error.tpl');
	}
}
?>
