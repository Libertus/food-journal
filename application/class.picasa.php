<?php

require_once('class.photo.php');

class picasaFeedDocument extends DOMXPath
{
	public function __construct( DOMDocument $picasaFeedDoc )
	{
		parent::__construct( $picasaFeedDoc );
		$this->registerNamespace( 'atom', 'http://www.w3.org/2005/Atom' );
		$this->registerNamespace( 'media', 'http://search.yahoo.com/mrss/' );
	}
}

class picasa
{

	public function getProviderDetails( User $user )
	{
		$details = $user->getProviderDetails();

		$details['provider'] = 'Picasa Web Albums';
		$details['sourcephotos'] =
			'http://picasaweb.google.com/'
			. urlencode($details['username'] ? $details['username'] : $user->getUsername())
		;

		$user->setProviderDetails($details);
	}

	public function getPhotostream( $user )
	{
		return
			$this->organisePhotostream(
				$this->extractPhotosFromAlbums(
					$this->loadAlbumsFromUserFeed(
						$this->loadUserFeed(
							$user
						)
					)
				)
			)
		;
	}

	protected function loadUserFeed( User $user )
	{
		// load the user's feed, which yields their personal details
		// and album identifiers
		$urlUserFeed = 'http://picasaweb.google.co.uk/data/feed/base/user/';
		$picasaUserDetails = $user->getProviderDetails();
		$urlUserFeed .= $user->getUsername();
		$urlUserFeed .= '?alt=atom';
		$docUserFeed = self::loadPicasaFeed( $urlUserFeed );

		// extract the personal details
		$user->setDisplayname(
			$docUserFeed->evaluate(
				'string(/atom:feed/atom:author/atom:name)'
			)
		);

		return $docUserFeed;
	}

	protected function loadAlbumsFromUserFeed( picasaFeedDocument $docUserFeed )
	{
		$albumEntryQuery =
				'/atom:feed/atom:entry[atom:category[@scheme="http://schemas.google.com/g/2005#kind" and @term="http://schemas.google.com/photos/2007#album"]]/atom:link[@rel="http://schemas.google.com/g/2005#feed" and @type="application/atom+xml"]/@href';
		foreach( $docUserFeed->query($albumEntryQuery) as $albumLinkNode ) {
			$albumFeeds[] = self::loadPicasaFeed( $albumLinkNode->value );
		}
		return (array)$albumFeeds;
	}

	protected function extractPhotosFromAlbums( array $albumFeedDocs )
	{
		$photoEntryQuery =
				'/atom:feed/atom:entry[atom:category[@scheme="http://schemas.google.com/g/2005#kind" and @term="http://schemas.google.com/photos/2007#photo"]]';
		foreach( $albumFeedDocs as $docAlbumFeed )
		{
			$entries = $docAlbumFeed->query($photoEntryQuery);
			foreach( $entries as $photoEntryNode )
			{
				// extract date taken from the HTML summary blob in the feed
				$summary = $docAlbumFeed->evaluate('string(atom:summary[@type="html"])', $photoEntryNode);

				$summaryXml = simplexml_load_string(
					stripslashes($summary) // wtf? <elem attr=\"value\"> isn't HTML!
				);

				$taken = self::convertPicasaDateToUTC( $summaryXml->tr->td[1]->font[1] );

				$photo = new Photo(
					$taken->format('U')
				,	$docAlbumFeed->evaluate('string(media:group/media:thumbnail[@width=72 or @height=72]/@url)', $photoEntryNode)// thumbnail
				,	$docAlbumFeed->evaluate('string(media:group/media:thumbnail[@width=288 or @height=288]/@url)', $photoEntryNode)// picture
				,	$taken->format('H:i ')
				.	$docAlbumFeed->evaluate('string(atom:title[@type="text"])', $photoEntryNode)// title
				,	$docAlbumFeed->evaluate('string(atom:link[@rel="alternate" and @type="text/html"]/@href)', $photoEntryNode)// link
				);
				$photos[] = $photo;
			}
		}

		return (array)$photos;
	}

	protected function organisePhotostream( array $photos )
	{
		// sort by timestamp taken
		usort( $photos, create_function(
			'$a,$b'
			, 'return $a->getTimestampTaken() - $b->getTimestampTaken();'
		) );
		return $photos;
	}

	private static function loadPicasaFeed( $url )
	{
		return new picasaFeedDocument( DOMDocument::load( $url, LIBXML_COMPACT ) );
	}

	/// @return DateTime
	private static function convertPicasaDateToUTC( $picasaDate )
	{
		// Google operates out of California
		static $tzPicasa = null, $tzUTC;
		if( null === $tzPicasa ) {
			$tzPicasa = new DateTimeZone('America/Los_Angeles');
			$tzUTC = new DateTimeZone('UTC');
		}

		$d = new DateTime($picasaDate, $tzPicasa);
		$d->setTimezone($tzUTC);
		return $d;
	}

}

?>