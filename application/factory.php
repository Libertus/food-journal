<?php

function createProvider(User $user) {
	
	$providername = getProvider($user);
	
	$classurl = "class.".$providername.".php";
	
	$offroot = realpath(dirname(__FILE__));
	
	if (file_exists($offroot.'/'.$classurl)) {	
		include $classurl;
		$obj = new $providername;
		return $obj;	
	}	 
}
function getProvider (User $user) {
	return $user->getProvider();
}

?>