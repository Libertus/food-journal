<?php

interface photostream {
	/**
	* This method returns the title of the photostream provider which  
	* which can be used in user output
	* 
	* @returns string
	*/
	public function __toString();
	
	/** 
	* This method checks user exists on the photostream provider 
	* 
	* @params string
	* @returns boolean
	*/
	public function loadUserDetails( User $user) ;

	/**
	* This method returns user details and photostream stored by days
	* 
	* @params string
	* @returns array
	*/
	public function getPhotostreamDay( User $user, $date);	

	/**
	* This method returns user details and photostream stored by week
	* 
	* @params string
	* @returns array
	*/
	public function getPhotostreamWeek( User $user, $date);	
}

?>