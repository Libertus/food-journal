<?php
// function.photostream.php



function filterPhotostream($photostream,$date_start,$daybreak,$range='') {

	$start = strtotime($date_start.' '.$daybreak.':00Z');
	if('day'==$range) { // single day to be returned
		$end = $start + (24 * 60 * 60) - 1;
		$nav = calcNavigation($photostream,$start,$daybreak,$range);
	} else {
		// return a week
		$end = $start + (7 * 24 * 60 * 60) - 1;
		$nav = calcNavigation($photostream,$start,$daybreak,$range);
	}

	$photostream = cutToRange($photostream,$start,$end);

	// default week display x=day,y=hour but as it is written in table by rows, top level sort is y axis : hours
	if('day'==$range) {
		$photostream['photos'] = sortIntoDayBuckets($photostream,$start,$end,$daybreak);
	} else {
		$photostream['photos'] = sortIntoHourBuckets($photostream,$start,$end,$daybreak);
	}

	$photostream['nav'] = $nav;

	return $photostream;
}


function cutToRange($photostream,$start,$end) {

	foreach ($photostream as $photo ) {
		$datetaken = $photo->getTimestampTaken();
		if($datetaken >= $start  and $datetaken <= $end) $filter[] = $photo;
	}


	return $filter;
}

function sortIntoDayBuckets($photostream,$start,$end,$daybreak) {

	// puts 'next day' photos into this day if daybreak later than midnight
	$timeshift = substr($daybreak,0,2);
	$start -= $start % (60 * 60 * 24);
	$end -= $end % (60 * 60 * 24);

	// prepare empty buckets for each hour on each day in range
	for ($day = $start; $day < $end; $day+=(60 * 60 * 24) ) {
		for( $hour = 0; $hour < 24; $hour++ ) {
			$hour_key = sprintf('%02d:00', ($hour+$timeshift)%24);
			$photos_by_day[$day][$hour_key] = array();
		}
	}

	foreach( (array) $photostream as $photo ) {
		// use timeshift to put photo into correct day bucket
		$day_start_timestamp = $photo->getTimestampTaken() - ($timeshift * 60 * 60);
		$day_start_timestamp -= $day_start_timestamp % (60 * 60 * 24);

		$hour = $photo->getHourTaken();

		$photos_by_day[$day_start_timestamp][$hour][] = $photo;
	}

	return $photos_by_day;

}

function sortIntoHourBuckets($photostream,$start,$end,$daybreak) {

	// puts 'next day' photos into this day if daybreak later than midnight
	$timeshift = substr($daybreak,0,2);
	$start -= $start % (60 * 60 * 24);
	$end -= $end % (60 * 60 * 24);

	// prepare empty buckets for each hour on each day in range
	for( $hour = 0; $hour < 24; $hour++ ) {
		for ($day = $start; $day < $end; $day+=(60 * 60 * 24) ) {
			$hour_key = sprintf('%02d:00', ($hour+$timeshift)%24);
			$photos_by_day[$hour_key][$day] = array();
		}
	}

	foreach( (array) $photostream as $photo ) {
		// use timeshift to put photo into correct day bucket
		$day_start_timestamp = $photo->getTimestampTaken() - ($timeshift * 60 * 60);
		$day_start_timestamp -= $day_start_timestamp % (60 * 60 * 24);

		$hour = $photo->getHourTaken();

		$photos_by_day[$hour][$day_start_timestamp][] = $photo;
	}

	return $photos_by_day;
}

function photoNavigation($photostream) {
	$navigation = array();
	foreach( $photostream as $photo ) {
		$day_start_timestamp = $photo->getTimestampTaken();
		$day_start_timestamp -= $day_start_timestamp % (60 * 60 * 24);
		$year = date('Y',$day_start_timestamp);
		$weekno = date('W',$day_start_timestamp);
		$navigation[$year][$weekno]['count']++;
		$navigation[$year][$weekno]['days'][$day_start_timestamp]++;
		$day_of_week = date('N',$day_start_timestamp);
		if(!$navigation[$year][$weekno]['monday']) {
			$navigation[$year][$weekno]['monday'] = $day_start_timestamp - (($day_of_week-1) * 60 * 60 * 24);

		}
		$weeks[$day_start_timestamp - (($day_of_week-1) * 60 * 60 * 24)]++;
		$days[$day_start_timestamp]++;
	}
	//echo '<pre>';
	//echo 'nav';
	//print_r($navigation);
	//echo '</pre>';
	//die();


	return $navigation;
}

function daybreakNavigation() {
	return array ('00:01'=>'12 midnight',
					'01:00'=>'1am',
					'02:00'=>'2am',
					'03:00'=>'3am',
					'04:00'=>'4am',
					'05:00'=>'5am',
					'06:00'=>'6am',
					'07:00'=>'7am',
					'08:00'=>'8am',
					'09:00'=>'9am',
					'10:00'=>'10am',
					'11:00'=>'11am',
					'12:00'=>'12 midday',
					'13:00'=>'1pm',
					'14:00'=>'2pm',
					'15:00'=>'3pm',
					'16:00'=>'4pm',
					'17:00'=>'5pm',
					'18:00'=>'6pm',
					'19:00'=>'7pm',
					'20:00'=>'8pm',
					'21:00'=>'9pm',
					'22:00'=>'10pm',
					'23:00'=>'11pm');
}

function calcNavigation($photostream,$start,$daybreak,$range) {
	// adjust for daybreak
	$start -= $start % (60 * 60 * 24);
	foreach( $photostream as $photo ) {
		$day_start_timestamp = $photo->getTimestampTaken();
		$day_start_timestamp -= $day_start_timestamp % (60 * 60 * 24);
		if('day'==$range) {
			$entries[$day_start_timestamp]++;
		} else {
			$day_of_week = date('N',$day_start_timestamp);
			$entries[$day_start_timestamp - (($day_of_week-1) * 60 * 60 * 24)]++;
		}
	}

	if('day'==$range) {
		// calculate the week its in
		$day_of_week = date('N',$start);
		$nav['this']['week'] = $start - (($day_of_week-1) * 60 * 60 * 24);
	}

	// set navigation parameters
	$nav['this']['start'] = $start;
	$nav['daybreak'] = $daybreak;
	$nav['daybreaknav'] = daybreakNavigation();
	$nav['range'] = $range;

	$entrylist = array_keys($entries);
	$key = array_search($start,$entrylist);
	if(false == $key) {
		// key is not found, search for nearest
		if($start > $entrylist[count($entrylist) - 1]) { // after latest data entry
			$key += count($entrylist) ;
		} elseif ($start < $entrylist[0] ) { // before earliest data entry
			$key = -1;
		} else { // in the middle somewhere with an empty day or week
			for ($i=0;$i<count($entrylist);$i++) {
				if($start < $entrylist[$i] and $start > $entrylist[$i -1]) {
					$key = $i - 0.5;
					break;
				}
			}
		}
	}
	if ($key <> ceil($key)) { // middle ground - have to set prev and next
		$nav['prev']['start'] = $entrylist[$key - 0.5];
		$nav['prev']['count'] = $entries[ $entrylist[$key - 0.5] ];
		$nav['next']['start'] = $entrylist[$key + 0.5];
		$nav['next']['count'] = $entries[ $entrylist[$key + 0.5] ];
		$nav['this']['count'] = o;

	} else {
		$nav['this']['count'] = $entries[$entrylist[$key]];
		if(0<$key) {
			$nav['prev']['start'] = $entrylist[$key - 1];
			$nav['prev']['count'] = $entries[ $entrylist[$key - 1] ];
		}
		if(count($entrylist) > $key) {
			$nav['next']['start'] = $entrylist[$key + 1];
			$nav['next']['count'] = $entries[ $entrylist[$key + 1] ];
		}
	}

	return $nav;
}
?>